<?php

namespace App\Http\Controllers;

use App\Notebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotebooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $notebooks = $user->notebooks;
        return view('notebooks.index', compact('notebooks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('notebooks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      $input =  $request->all();

      $user = Auth::user();
      $user->notebooks()->create($input);
      //Notebook::create($input);

      return redirect()->route('notebooks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notebook = Notebook::findOrFail($id);

        $notes = $notebook->notes();

        return view('notes.index', compact('notes', 'notebook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = Auth::user();
        $notebook = $user->notebooks()->findOrFail($id);
        //$notebook = Notebook::findOrFail($id);

        return view('notebooks.edit', compact('notebook'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //$notebook = Notebook::findOrFail($id);
        $user = Auth::user();
        $notebook = $user->notebooks()->findOrFail($id);
        $notebook->update($request->all());

        return redirect()->route('notebooks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = Auth::user();
        $user->notebooks()->findOrFail($id)->delete();
        //Notebook::findOrFail($id)->delete();

        return redirect()->route('notebooks.index');
    }
}
