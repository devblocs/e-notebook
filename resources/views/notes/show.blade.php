@extends('layouts.main')

@section('title', 'Notebooks')
@php
  $currentpage = 'notes';
@endphp
@section('content')
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
          <h2>{{ title_case($note->title) }}</h2>
          <p>{{ ucfirst($note->body) }}</p>
        </div>
      </div>
    </div>

    <div class="row">
      <p></p>
    </div>
    </div><!-- end content -->
  </div>
@endsection
