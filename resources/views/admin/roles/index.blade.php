@extends('layouts.admin.adminMain')
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h3>Roles</h3>
      <a href="{{ route('roles.create') }}" class="btn btn-primary pull-right">Create Role</a>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Display Name</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            @forelse ($roles as $role)
              <tr>
                <td>{{ $role->name }}</td>
                <td>{{ $role->display_name }}</td>
                <td>{{ $role->description }}</td>
              </tr>
            @empty
              <tr>
                <td></td>
                <td>No Roles Found</td>
                <td></td>
              </tr>
            @endforelse
          </tbody>
        </table>
      </div>
  </div>

@endsection
