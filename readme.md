# E-notebook
An online note taking app

## Project Description
E-notebook is an online web app for storing notes in an organized collections called notebooks. The inspiration of the this web app is Microsoft onedrive. The project was intended to collects notes for college student in digital form, so that they can avoid carrying books. With the use digital notes students can have the flexibility of reading the class notes wherever they go, all they need is good internet connection for there handheld devices.

## Tech Specifications
- **Front-end:**
    - HTML5
    - CSS3
    - JavaScript
    - jQuery
    - Materializecss

- **Back-end:**
    - Laravel(PHP)
    - MySQL

### Future Updates:
    1. Upgrading the project 
    2. Multi-auth system 
    3. Request for contribution to the notes.
    4. Access Control List(ACL) implementation for notes.
    5. Sharing notes with co-students.
