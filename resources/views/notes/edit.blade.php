@extends('layouts.main')

@section('title', 'Create Notes')

@php
  $currentpage = 'notes';
@endphp

@section('content')
  <div class="container">
    <div class="row">
      <h2>Edit Note</h2>
    </div>

  <div class="row">
    <form class="col s12" action="{{ route('notes.update', $note->id) }}" method="POST">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <div class="row">
        <div class="input-field col s12">
          <input id="title" type="text" class="validate" name="title" value="{{ $note->title }}">
          <label for="title">Note Title:</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea id="body" name="body" class="materialize-textarea">{{ $note->body }}</textarea>
          <label for="body">Description</label>
        </div>
      </div>
      <input type="hidden" name="notebook_id" value="{{ $note->notebook_id }}">
      <div class="center-align">
      <button class="btn waves-effect waves-light blue darken-2" type="submit" name="submit">Update Note</button>
    </div>
    </form>
  </div>
  </div>

  @if ($errors->any())
      <div class="row">
        <div class="col s12 m12 l12">
          <div class="card-panel red lighten-1">
            <ul>
              @foreach ($errors->all() as $error)
                <li class="black-text">{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
  @endif
@endsection
