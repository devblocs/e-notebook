@extends('layouts.main')

@section('title', 'Notes')

@php
  $currentpage = 'notes';
@endphp

@section('content')
  <!-- Main component for call to action -->
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
          <h2 class>{{ title_case($notebook['name']) }}</h2>
        </div>
      </div>
    </div>
    @if (count($notes) > 0)
      @foreach ($notes as $note)
        <div class="row">
          <div class="col s12 m12 l12">
            <div class="card">
              <div class="card-content black-text">
                <span class="card-title">{{ title_case($note->title) }}</span>
                <p>{{ ucfirst($note->body) }}</p>
              </div>
              <div class="card-action">
                <div class="row center">
                  <div class="col s4 m4 l4">
                    <a class="waves-effect waves-light btn blue darken-2" href="{{ route('notes.show', $note->id) }}">View Note</a>
                  </div>
                  <div class="col s4 m4 l4">
                    <a class="waves-effect waves-light btn blue darken-2" href="{{ route('notes.edit', $note->id) }}">Edit Note</a>
                  </div>
                  <div class="col s4 m4 l4">
                    <form action="{{ route('notes.destroy', $note->id) }}" class="pull-s6" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                        <input type="hidden" name="notebook_id" value="{{ $note->notebook_id }}">

                        <button class="waves-effect waves-light btn red darken-2 round" type="submit" name="submit" onclick="Materialize.toast('Note Deleted successfully', 4000)">
                          Delete
                        </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
        <div class="fixed-action-btn">
          <a class="waves-effect waves-light btn-floating btn-large blue darken-2 tooltipped" href="{{ route('notes.create', $note->notebook_id) }}" data-position="left" data-delay="50" data-tooltip="Create Note">
            <i class="large material-icons">add</i>
          </a>
        </div>
  @else
    <h3>no notes found</h3>
    <div class="fixed-action-btn">
      <a class="waves-effect waves-light btn-floating btn-large blue darken-2 tooltipped" href="{{ route('notes.create', $notebook->id) }}" data-position="left" data-delay="50" data-tooltip="Create Note">
        <i class="large material-icons">add</i>
      </a>
    </div>
  @endif

  </div> <!-- /container -->
@endsection
