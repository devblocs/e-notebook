@extends('layouts.main')

@section('title', 'Notebooks')
@php
  $currentpage = 'notebook';
@endphp
@section('content')
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="col s12 m12 l12">
          <h2 class>Your Notebooks</h2>
        </div>
      </div>
    </div>

    <!-- Notebook content -->
    <div class="row">
      @if ($notebooks == true)
        @foreach ($notebooks as $notebook)
          <div class="col s12 m4 l4">
            <div class="card">
            <div class="card-image">
              <img src="img/notebook.jpg">
              <span class="card-title">{{ title_case($notebook->name) }}</span>
              <a class="btn-floating halfway-fab waves-effect waves-light blue darken-2 btn-large" href="{{ route('notebooks.edit', $notebook->id) }}"><i class="material-icons">create</i></a>
            </div>
            <div class="card-content">
              <p>I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.</p>
            </div>
            <div class="card-action">
              <div class="row">
                <div class="col m6 l6">
                  <a class="waves-effect waves-light btn blue darken-2" href="{{ route('notes.index', $notebook->id) }}">View Notes</a>
                </div>
                <div class="col m6 l6">
                  <form action="{{ route('notebooks.destroy', $notebook->id) }}" class="pull-s6" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="waves-effect waves-light btn red darken-2 round" type="submit" name="submit" onclick="Materialize.toast('Notebook Deleted successfully', 4000)">
                      Delete
                    </button>
                  </form>
                </div>
              </div>


            </div>
          </div>
          </div>
        @endforeach
      @endif
    </div><!-- end content -->
  </div>
  <div class="fixed-action-btn">
    <a class="waves-effect waves-light btn-floating btn-large blue darken-2 tooltipped" href="{{ route('notebooks.create') }}" data-position="left" data-delay="50" data-tooltip="Create Notebook">
      <i class="large material-icons">add</i>
    </a>
  </div>
@endsection
