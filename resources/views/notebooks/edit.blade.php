@extends('layouts.main')

@section('title', 'Create Notebook')
@php
  $currentpage = 'notebookAction';
@endphp
@section('content')
  <div class="container">
    <div class="row">
      <h2>Update Notebook</h2>
    </div>

    <div class="row">
      <form class="col s12" action="{{ route('notebooks.update', $notebook->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="row">
          <div class="input-field col s12">
            <input id="name" type="text" class="validate" name="name" value="{{ $notebook->name }}">
            <label for="name">Notebook Name:</label>
          </div>
        </div>
        <div class="center-align">
          <button class="btn waves-effect waves-light blue darken-2" type="submit" name="submit">Update Notebook</button>
        </div>
      </form>
    </div>
  </div>
@endsection
