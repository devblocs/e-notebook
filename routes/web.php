<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware('auth')->group(function(){
  Route::get('/', function (){
      return view('index');
  });

  Route::get('/notebooks/notes', function(){
    return view('notes.index');
  });

  Route::resource('/notebooks', 'NotebooksController');

  Route::resource('/notes', 'NotesController', ['except' => [
    'index', 'create'
  ]]);

  Route::get('/notebooks/{id}/notes', 'NotesController@index')->name('notes.index');

  Route::get('/notes/{notebookId}/create', 'NotesController@create')->name('notes.create');

  Route::resource('/admin', 'AdminController');

  Route::resource('/roles', 'RoleController');
  //Route::get('notes/{notebookId}/createNote', 'NotesController@createNote')->name('notes.createNote');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
