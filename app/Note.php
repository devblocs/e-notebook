<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //
    protected $fillable = ['title', 'body', 'notebook_id'];

    /**
     * Get the notebook that owns the model.
     */
    public function notebook()
    {
        return $this->belongsTo('App\Notebook');
    }
}
