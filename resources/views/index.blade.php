@extends('layouts.main')

@section('title', 'Home')
@php
  $currentpage = 'index'
@endphp
@section('content')
  <!-- Main component for call to action -->
  <div class="container">
    <div class="row">
      <h1>Notebook</h1>
      <p>Store and organise your thoughts in notebook and NoteBook web app makes this easier than ever</p>
      <p>
          <a class="waves-effect waves-light btn-large blue darken-2" href="{{ route('notebooks.index') }}"><i class="material-icons right">book</i>Your NoteBooks</a>
      </p>
    </div>
  </div>  <!-- /container -->
@endsection
