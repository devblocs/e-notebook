@extends('layouts.main')

@section('title', 'Create Notes')

@php
  $currentpage = 'notes';
@endphp

@section('content')
  <div class="container">
    <div class="row">
      <h2>Create Note</h2>
    </div>

      <div class="row">
        <form class="col s12" action="{{ route('notes.store') }}" method="POST">
          {{ csrf_field() }}
          <div class="row">
            <div class="input-field col s12">
              <input id="title" type="text" class="validate" name="title">
              <label for="title">Note Title:</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="body" name="body" class="materialize-textarea"></textarea>
              <label for="body">Description</label>
            </div>
          </div>
          <input type="hidden" name="notebook_id" value="{{ $id }}">
          <div class="center-align">
          <button class="btn waves-effect waves-light blue darken-2" type="submit" name="submit">Create Note</button>
        </div>
        </form>
      </div>

      @if ($errors->any())
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card-panel red lighten-1">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li class="black-text">{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
      @endif
  </div>
@endsection
