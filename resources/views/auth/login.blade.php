@extends('layouts.main')

@section('title', 'Login')

@section('content')
<div class="container">
        <div class="card-panel">
          <div class="row">
            <h2 class="center-align">Login</h2>
          </div>

          <div class="row">
            <div class="col offset-s2">

            </div>
            <form class="col s8" action="{{ route('login') }}" method="POST">
              {{ csrf_field() }}
              <div class="row">
                <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                  <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                  <label for="email">Email</label>
                  @if ($errors->has('email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                  <input id="password" type="password" name="password" class="validate">
                  <label for="password">Password</label>
                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                  <input type="checkbox" class="filled-in" id="remember"  class="{{ old('remember') ? 'checked' : '' }}" />
                  <label for="remember">Remember Me</label>
                </div>
              </div>
              <div class="center-align">
                <div class="row">
                  <button class="btn waves-effect waves-light blue darken-2" type="submit" name="submit">Login</button>
                </div>
                <div class="row">
                  <a class="btn waves-effect waves-light blue darken-2" href="{{ route('password.request') }}">
                      Forgot Your Password?
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
</div>
@endsection
