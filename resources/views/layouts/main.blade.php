<!DOCTYPE html>
<html>
  <head>
      <meta charset="UTF-8">
      <title>NoteBook App | @yield('title')</title>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">

      <link rel="stylesheet" href="{{ asset('css/main.css') }}">

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <div class="navbar-fixed">
        <nav class="blue darken-2" role="navigation">
          <div class="nav-wrapper container">
            <div class="col s12">
            <a href="/" class="brand-logo">Notebook App</a>
            <ul class="right hide-on-med-and-down">
              <!-- Authentication Links -->
              @if (Auth::guest())
                  <li><a href="{{ route('login') }}">Login</a></li>
                  <li><a href="{{ route('register') }}">Register</a></li>
              @else
                @if ($currentpage == 'index')
                  <li class="active"><a href="#!">Home</a></li>
                @else
                  <li><a href="/">Home</a></li>
                @endif

                @if ($currentpage == 'notebook')
                  <li class="active"><a href="#!">Notebooks</a></li>
                @elseif ($currentpage == 'notebookAction')
                  <li class="active"><a href="{{ route('notebooks.index') }}">Notebooks</a></li>
                @else
                  <li><a href="{{ route('notebooks.index') }}">Notebooks</a></li>
                @endif

                <li><a class="dropdown-button" href="#!" data-activates="dropdown">{{ ucfirst(Auth::user()->name) }}<i class="material-icons right">arrow_drop_down</i></a></li>
              @endif

            </ul>

            <!-- Mobile Navigation Menu -->
            <ul id="nav-mobile" class="side-nav">
              <!-- Authentication Links -->
              @if (Auth::guest())
                  <li><a href="{{ route('login') }}">Login</a></li>
                  <li><a href="{{ route('register') }}">Register</a></li>
              @else
                  <li><a href="/">Home</a></li>
                  <li><a href="{{ route('notebooks.index') }}">Notebooks</a></li>
                  <li><a class="dropdown-button" href="#!" data-activates="dropdown1">{{ ucfirst(Auth::user()->name) }}<i class="material-icons right">arrow_drop_down</i></a></li>
              @endif
            </ul>


            <a href="#" data-activates="nav-mobile" class="button-collapse right right-nav"><i class="material-icons">menu</i></a>
          </div>
          </div>
        </nav>
          <!-- Dropdown Structure for desktop -->
        <ul id="dropdown" class="dropdown-content">
        <li class="divider"></li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
        </ul>

        <!-- Dropdown Structure for mobile -->
        <ul id="dropdown1" class="dropdown-content">
          <li class="divider"></li>
          <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          </li>
        </ul>
    </div>

    @yield('content')

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

        $(".dropdown-button").dropdown({
          inDuration: 300,
          outDuration: 225,
          constrain_width: false, // Does not change width of dropdown to that of the activator
          hover: true, // Activate on click
          alignment: 'left', // Aligns dropdown to left or right edge (works with constrain_width)
          gutter: 1, // Spacing from edge
          belowOrigin: false // Displays dropdown below the button
        });

        $(".button-collapse").sideNav('show');



      $('.right-nav').sideNav({
          menuWidth: 300, // Default is 300
          edge: 'right', // Choose the horizontal origin
          closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
          draggable: true // Choose whether you can drag to open on touch screens
        });

  });
    </script>
  </body>
</html>
