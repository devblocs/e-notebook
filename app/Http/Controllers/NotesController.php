<?php

namespace App\Http\Controllers;

use App\Notebook;
use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $notebook = Notebook::findOrFail($id);

        $notes = $notebook->notes;

        //return $notes;
        return view('notes.index', compact('notes', 'notebook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($notebookId)
    {
        //
      return view('notes.create')->with('id', $notebookId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
          'title' => 'required',
          'body' => 'required',
        ]);

        Note::create($request->all());

        return redirect()->route('notes.index', $request->notebook_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $note = Note::findOrFail($id);

        return view('notes.show')->with('note', $note);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $note = Note::findOrFail($id);

        return view('notes.edit')->with('note', $note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
          'title' => 'required',
          'body' => 'required',
        ]);

        $note = Note::findOrFail($id);
        $note->update($request->all());

        return redirect()->route('notes.index', $request->notebook_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        Note::findOrFail($id)->delete();

        return redirect()->route('notes.index', $request->notebook_id);
    }
}
