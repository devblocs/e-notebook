<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notebook extends Model
{
    //
    protected $fillable = ['name'];

    /**
     * Get the notes for the model.
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }
}
